// Copyright: TungTran2022
// MQTT communication for transmit PID params to STM32 to control motor

#include <Arduino.h>
#include <WiFiClient.h>
#include <PubSubClient.h>
#include <painlessMesh.h>
#include <SoftwareSerial.h>

SoftwareSerial mySerial(D7, D8); // RX, TX
WiFiClient wifiClient;
PubSubClient mqttClient(wifiClient);
// D7 RX - PA9
// D8 TX - PA10
// (Send and Receive)
const char *mqtt_server = "m14.cloudmqtt.com";
const char *mqtt_username = "bxpalvco";
const char *mqtt_password = "UUILhS73phGV";
const char *clientID = "TUNG";
const char *endTopic = "phuongtung0801/LWT";
long int port = 12321;

unsigned long times = millis();
float slider;
float Kp;
float Ki;
float Kd;
byte data[7];
float number;
char numberStringSend[6];

void mqttCallback(char *topic, uint8_t *payload, unsigned int length);

void setup()
{
  Serial.begin(9600);
  mySerial.begin(9600);
  WiFi.begin("TUNG", "123456789");
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(1000);
    Serial.println("Connecting to WiFi..");
  }
  Serial.println(WiFi.localIP());
  mqttClient.setServer(mqtt_server, port);
  mqttClient.setCallback(mqttCallback);
  if (mqttClient.connect(
          clientID, mqtt_username, mqtt_password, endTopic, 1, true, "Sensor disconnected from mqtt"))
  {
    Serial.println("Connected to MQTT Broker!");
  }
  else
  {
    Serial.println("Connection to MQTT Broker failed...");
  }
  mqttClient.subscribe("tungtran"); // test topic
  // subscribe vào các Topic cần gửi lệnh xuống STM32
  mqttClient.subscribe("Mode");
  mqttClient.subscribe("Start");
  mqttClient.subscribe("Speed");
  mqttClient.subscribe("Kp");
  mqttClient.subscribe("Kd");
  mqttClient.subscribe("Ki");
}

void loop()
{
  mqttClient.loop();
  if (Serial.available() > 0)
  { // Read from serial monitor and send
    String input = Serial.readString();
    mySerial.println(input);
  }
  delay(200);

  if (mySerial.available() > 1)
  {
    mySerial.readBytes(data, 1);
    if (data[0] == 'R')
    {
      if (mySerial.available() > 5)
      { // Read from  STM module and send to serial monitor
        mySerial.readBytes(data + 1, 6);

        Serial.println(data[1]);
        Serial.println(data[2]);
        Serial.println(data[3]);
        Serial.println(data[4]);
        Serial.println(data[5]);
        number = data[1] * 100 + data[2] * 10 + data[3] * 1 + data[4] * 0.1 + data[5] * 0.01;
        if (data[6] == '-')
        {
          number = 0 - number;
        }
        dtostrf(number, 4, 2, numberStringSend);
      }
      mqttClient.publish("real speed", numberStringSend);
    }
  }
}

void mqttCallback(char *topic, uint8_t *payload, unsigned int length)
{

  String cmd = "";
  Serial.print("Message arrived [");
  Serial.print(topic);

  Serial.print("] ");
  for (int i = 0; i < length; i++)
  {
    cmd += (char)payload[i];
  }
  DynamicJsonDocument jsonBuffer(1024);
  JsonObject msg = jsonBuffer.to<JsonObject>();
  msg["topic"] = topic;
  msg["msg"] = cmd;
  String str;
  serializeJson(msg, str);
  // Send payload to STM32 through UART
  if (strcmp(topic, "Mode") == 0)
  {
    int mode = cmd.toInt();
    Serial.printf("Mode is: %d", mode);
    mySerial.print("A");
    byte *b = (byte *)&mode;
    mySerial.write(b, 4);
    return;
  }

  if (strcmp(topic, "Start") == 0)
  {
    int start = cmd.toInt();
    Serial.printf("Start is: %d", start);
    mySerial.print("E");
    byte *b = (byte *)&start;
    mySerial.write(b, 4);
    return;
  }

  if (strcmp(topic, "Speed") == 0)
  {
    float speed = cmd.toFloat();
    Serial.print("Speed is: ");
    mySerial.print("S");
    byte *b = (byte *)&speed;
    mySerial.write(b, 4);
    return;
  }

  if (strcmp(topic, "Kp") == 0)
  {
    float p = cmd.toFloat();
    Serial.printf("Kp is: %f", p);
    mySerial.print("P");
    byte *b = (byte *)&p;
    mySerial.write(b, 4);
    return;
  }
  if (strcmp(topic, "Ki") == 0)
  {
    float i = cmd.toFloat();
    Serial.printf("Ki is: %f", i);
    mySerial.print("I");
    byte *b = (byte *)&i;
    mySerial.write(b, 4);
    return;
  }
  if (strcmp(topic, "Kd") == 0)
  {
    float d = cmd.toFloat();
    Serial.printf("Kd is: %f", d);
    mySerial.print("D");
    byte *b = (byte *)&d;
    mySerial.write(b, 4);
    return;
  }
}
